using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using nrgApi.Database;
using nrgApi.Database.UserManagement;
using nrgApi.Models.Database;
using Xunit;
using Xunit.Sdk;

namespace NrgApi.Tests.Database
{
  public class MockDatabase
  {
    // Mock user details
    protected const string MockUsername = "MOCK_USER";
    protected const string MockPassword = "MOCK_PASSWORD";
    protected const int MockContactId = 1;
    protected const string MockFirstName = "MOCK_FIRSTNAME";
    protected const string MockLastName = "MOCK_LASTNAME";
    protected const string MockHouseNumberOrName = "743";
    protected const string MockStreet = "Evergreen Terrace";
    protected const string MockTown = "Springfield";

    // Mock account details
    protected const string MockAccountId = "MOCK_123";
    protected const decimal MockCostPerMonth = 102.10m;
    protected const bool MockPayingEnough = false;
    protected const decimal MockEndOfPlanBalance = 110.00m;
    protected const int MockElectricityMeterReading = 123456;
    protected const int MockGasMeterReading = 987654;
    
    // Mock energy usage
    protected const int MockEnergyUsageId = 1;

    internal readonly NrgDbContext DbContext;

    protected MockDatabase(string databaseName)
    {
      var dbContextOptions = new DbContextOptionsBuilder().UseInMemoryDatabase(databaseName);
      DbContext = new NrgDbContext(dbContextOptions.Options);
      SeedMockData();
    }
    
    protected bool DoLogin()
    {
      // Given
      var userManager = new UserManager(DbContext);

      // When
      return userManager.Login(MockUsername, MockPassword);
    }

    private void SeedMockData()
    {
      if (DbContext.Users.Any())
      {
        return; // Data was already seeded
      }

      DbContext.Users.AddRange(
        new UserDetails
        {
          Username = MockUsername,
          Password = DbContext.HashPassword(MockPassword),
          ContactDetails = new Contact
          {
            ContactId = MockContactId,
            FirstName = MockFirstName,
            LastName = MockLastName,
            HouseNumberOrName = MockHouseNumberOrName,
            Street = MockStreet,
            Town = MockTown
          },
          AccountDetails = new AccountDetails
          {
            AccountId = MockAccountId,
            CostPerMonth = MockCostPerMonth,
            PayingEnough = MockPayingEnough,
            EndOfPlanBalance = MockEndOfPlanBalance,
            ElectricityMeterReading = MockElectricityMeterReading,
            GasMeterReading = MockGasMeterReading
          },
          EnergyUsage = new EnergyUsage
          {
            EnergyUsageId = MockEnergyUsageId,
            ElectricityCosts = new List<EnergyCost>
            {
              new EnergyCost
              {
                EnergyCostId = 1,
                MonthIndex = 1,
                Cost = 70
              },
              new EnergyCost
              {
                EnergyCostId = 2,
                MonthIndex = 2,
                Cost = 65
              },
              new EnergyCost
              {
                EnergyCostId = 3,
                MonthIndex = 3,
                Cost = 76
              },
              new EnergyCost
              {
                EnergyCostId = 4,
                MonthIndex = 4,
                Cost = 60
              },
              new EnergyCost
              {
                EnergyCostId = 5,
                MonthIndex = 5,
                Cost = 80
              }
            },
            GasCosts = new List<EnergyCost>
            {
              new EnergyCost
              {
                EnergyCostId = 6,
                MonthIndex = 1,
                Cost = 160
              },
              new EnergyCost
              {
                EnergyCostId = 7,
                MonthIndex = 2,
                Cost = 125
              },
              new EnergyCost
              {
                EnergyCostId = 8,
                MonthIndex = 3,
                Cost = 120
              },
              new EnergyCost
              {
                EnergyCostId = 9,
                MonthIndex = 4,
                Cost = 100
              },
              new EnergyCost
              {
                EnergyCostId = 10,
                MonthIndex = 5,
                Cost = 70
              }
            },
            SimilarElectricityCosts = new List<EnergyCost>
            {
              new EnergyCost
              {
                EnergyCostId = 11,
                MonthIndex = 1,
                Cost = 80
              },
              new EnergyCost
              {
                EnergyCostId = 12,
                MonthIndex = 2,
                Cost = 75
              },
              new EnergyCost
              {
                EnergyCostId = 13,
                MonthIndex = 3,
                Cost = 96
              },
              new EnergyCost
              {
                EnergyCostId = 14,
                MonthIndex = 4,
                Cost = 70
              },
              new EnergyCost
              {
                EnergyCostId = 15,
                MonthIndex = 5,
                Cost = 50
              }
            },
            SimilarGasCosts = new List<EnergyCost>
            {
              new EnergyCost
              {
                EnergyCostId = 16,
                MonthIndex = 1,
                Cost = 170
              },
              new EnergyCost
              {
                EnergyCostId = 17,
                MonthIndex = 2,
                Cost = 135
              },
              new EnergyCost
              {
                EnergyCostId = 18,
                MonthIndex = 3,
                Cost = 160
              },
              new EnergyCost
              {
                EnergyCostId = 19,
                MonthIndex = 4,
                Cost = 120
              },
              new EnergyCost
              {
                EnergyCostId = 20,
                MonthIndex = 5,
                Cost = 60
              }
            }
          }
        }
      );

      DbContext.SaveChanges();
    }
  }
}
