using nrgApi.Database.UserManagement;
using Xunit;

namespace NrgApi.Tests.Database.UserManagement
{
  public class UserManagerTest : MockDatabase
  {
    public UserManagerTest() : base("UserManagementDatabase")
    {
    }
    
    [Fact]
    public void CanLogin()
    {
      // Given

      // When
      var success = DoLogin();

      // Then
      Assert.True(success);
    }
    
    [Fact]
    public void HandleInvalidLogin()
    {
      // Given
      var userManager = new UserManager(DbContext);

      // When
      var success = userManager.Login(MockUsername, "INVALID");

      // Then
      Assert.False(success);
    }

    [Fact]
    public void HandleMissingUsernameInLogin()
    {
      // Given
      var userManager = new UserManager(DbContext);

      // When
      var success = userManager.Login(null, MockPassword);

      // Then
      Assert.False(success);
    }

    [Fact]
    public void HandleMissingPasswordInLogin()
    {
      // Given
      var userManager = new UserManager(DbContext);

      // When
      var success = userManager.Login(MockUsername, null);

      // Then
      Assert.False(success);
    }

    [Fact]
    public void CanLogout()
    {
      // Given
      var userManager = new UserManager(DbContext);

      // When
      var success = userManager.Logout(MockUsername);

      // Then
      Assert.True(success);
    }

    [Fact]
    public void HandleMissingUsernameInLogout()
    {
      // Given
      var userManager = new UserManager(DbContext);

      // When
      var success = userManager.Logout(null);

      // Then
      Assert.False(success);
    }

    [Fact]
    public void CanGetUserDetails()
    {
      // Given
      CanLogin();
      var userManager = new UserManager(DbContext);

      // When
      var userDetails = userManager.GetUserDetails(MockUsername);

      // Then
      Assert.NotNull(userDetails);
      Assert.Equal(userDetails.Username, MockUsername);
      Assert.Equal(userDetails.Password, DbContext.HashPassword(MockPassword));
      Assert.True(userDetails.LoggedIn);
      Assert.NotNull(userDetails.ContactDetails);
      Assert.Equal(userDetails.ContactDetails.ContactId, MockContactId);
      Assert.Equal(userDetails.ContactDetails.FirstName, MockFirstName);
      Assert.Equal(userDetails.ContactDetails.LastName, MockLastName);
      Assert.Equal(userDetails.ContactDetails.HouseNumberOrName, MockHouseNumberOrName);
      Assert.Equal(userDetails.ContactDetails.Street, MockStreet);
      Assert.Equal(userDetails.ContactDetails.Town, MockTown);
    }
  }
}
