using nrgApi.Database.AccountManagement;
using Xunit;

namespace NrgApi.Tests.Database.AccountManagement
{
  public class AccountManagerTest : MockDatabase
  {
    public AccountManagerTest() : base("AccountManagementDatabase")
    {
    }

    [Fact]
    public void CanReadAccountDetails()
    {
      // Given
      DoLogin();
      var accountManager = new AccountManager(DbContext);

      // When
      var accountDetails = accountManager.LookupAccountDetails(MockUsername);

      // Then
      Assert.NotNull(accountDetails);
      Assert.Equal(accountDetails.AccountId, MockAccountId);
      Assert.Equal(accountDetails.CostPerMonth, MockCostPerMonth);
      Assert.Equal(accountDetails.PayingEnough, MockPayingEnough);
      Assert.Equal(accountDetails.EndOfPlanBalance, MockEndOfPlanBalance);
      Assert.Equal(accountDetails.ElectricityMeterReading, MockElectricityMeterReading);
      Assert.Equal(accountDetails.GasMeterReading, MockGasMeterReading);
    }

    [Fact]
    public void CanReadEnergyUsageDetails()
    {
      // Given
      DoLogin();
      var accountManager = new AccountManager(DbContext);

      // When
      var energyUsage = accountManager.LookupEnergyUsage(MockUsername);

      // Then
      Assert.NotNull(energyUsage);
      Assert.Equal(energyUsage.EnergyUsageId, MockEnergyUsageId);
      Assert.NotEmpty(energyUsage.ElectricityCosts);
      Assert.NotEmpty(energyUsage.GasCosts);
      Assert.NotEmpty(energyUsage.SimilarElectricityCosts);
      Assert.NotEmpty(energyUsage.SimilarGasCosts);
    }
  }
}
