using System.Collections.Generic;
using System.Collections.Specialized;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Protocols;
using nrgApi.Helpers;
using Xunit;

namespace NrgApi.Tests.Helpers
{
  public class JwtHelperTest
  {
    [Fact]
    public void CanCreateJwtToken()
    {
      // Given
      var mockSettings = Options.Create(new AppSettings());
      mockSettings.Value.Secret = "A_MOCK_SECRET_VALUE_FOR_JWT_CREATION";
      var jwtHelper = new JwtHelper(mockSettings);
      
      // When
      var jwt = jwtHelper.CreateJwtToken("MOCK_USER");
      
      // Then
      Assert.NotNull(jwt);
    }
  }
}
