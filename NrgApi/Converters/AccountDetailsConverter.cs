using System.Linq;
using nrgApi.Models.Database;
using nrgApi.Models.JsonApi;

namespace nrgApi.Converters
{
  public static class AccountDetailsConverter
  {
    public static AccountDetailsResponse Convert(AccountDetails accountDetails)
    {
      return new AccountDetailsResponse
      {
        AccountId = accountDetails.AccountId,
        CostPerMonth = accountDetails.CostPerMonth,
        PayingEnough = accountDetails.PayingEnough,
        EndOfPlanBalance = accountDetails.EndOfPlanBalance,
        ElectricityMeterReading = accountDetails.ElectricityMeterReading,
        GasMeterReading =  accountDetails.GasMeterReading
      };
    }
    
    public static EnergyUsageResponse Convert(EnergyUsage energyUsage)
    {
      var response = new EnergyUsageResponse();
      
      response.ElectricityCosts.AddRange(energyUsage.ElectricityCosts
        .OrderBy(energyCost => energyCost.MonthIndex)
        .Select(energyCost => energyCost.Cost).ToList());
      
      response.GasCosts.AddRange(energyUsage.GasCosts
        .OrderBy(energyCost => energyCost.MonthIndex)
        .Select(energyCost => energyCost.Cost).ToList());
      
      response.SimilarElectricityCosts.AddRange(energyUsage.SimilarElectricityCosts
        .OrderBy(energyCost => energyCost.MonthIndex)
        .Select(energyCost => energyCost.Cost).ToList());
      
      response.SimilarGasCosts.AddRange(energyUsage.SimilarGasCosts
        .OrderBy(energyCost => energyCost.MonthIndex)
        .Select(energyCost => energyCost.Cost).ToList());

      return response;
    }
  }
}
