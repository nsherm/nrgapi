using nrgApi.Models.Database;
using nrgApi.Models.JsonApi;

namespace nrgApi.Converters
{
  public static class UserDetailsConverter
  {
    public static UserDetailsResponse Convert(UserDetails userDetails)
    {
      return new UserDetailsResponse
      {
        FirstName = userDetails.ContactDetails.FirstName,
        HouseNumberOrName = userDetails.ContactDetails.HouseNumberOrName,
        Street = userDetails.ContactDetails.Street,
        Town = userDetails.ContactDetails.Town
      };
    }
  }
}
