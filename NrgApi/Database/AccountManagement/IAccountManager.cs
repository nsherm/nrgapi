using nrgApi.Models.Database;

namespace nrgApi.Database.AccountManagement
{
  public interface IAccountManager
  {
    AccountDetails LookupAccountDetails(string username);

    EnergyUsage LookupEnergyUsage(string username);
  }
}
