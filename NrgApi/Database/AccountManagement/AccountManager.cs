using System.Linq;
using Microsoft.EntityFrameworkCore;
using nrgApi.Models.Database;

namespace nrgApi.Database.AccountManagement
{
  public class AccountManager : ManagerBase, IAccountManager
  {
    public AccountManager(NrgDbContext context) : base(context)
    {
    }

    public AccountDetails LookupAccountDetails(string username)
    {
      var userDetails = Context.Users
        .Include(c => c.AccountDetails)
        .Where(u => u.LoggedIn)
        .FirstOrDefault(u => u.Username == username );

      return userDetails?.AccountDetails;
    }
    
    public EnergyUsage LookupEnergyUsage(string username)
    {
      var userDetails = Context.Users
        .Include(c => c.EnergyUsage)
        .ThenInclude(electricityCosts => electricityCosts.ElectricityCosts)
        .Include(c => c.EnergyUsage)
        .ThenInclude(gasCosts => gasCosts.GasCosts)
        .Include(c => c.EnergyUsage)
        .ThenInclude(similarElectricityCosts => similarElectricityCosts.SimilarElectricityCosts)
        .Include(c => c.EnergyUsage)
        .ThenInclude(similarGasCosts => similarGasCosts.SimilarGasCosts)
        .Where(u => u.LoggedIn)
        .FirstOrDefault(u => u.Username == username);

      return userDetails?.EnergyUsage;
    }
  }
}
