using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using nrgApi.Models.Database;

namespace nrgApi.Database
{
  public static class DataGenerator
  {
    public static void Initialize(IServiceProvider serviceProvider)
    {
      using (var context = new NrgDbContext(
        serviceProvider.GetRequiredService<DbContextOptions<NrgDbContext>>()))
      {
        if (context.Users.Any())
        {
          return; // Data was already seeded
        }

        context.Users.AddRange(
          new UserDetails
          {
            Username = "user1",
            Password = context.HashPassword("password"),
            ContactDetails = new Contact
            {
              ContactId = 1,
              FirstName = "User 1",
              LastName = "Bloggs",
              HouseNumberOrName = "743",
              Street = "Evergreen Terrace",
              Town = "Springfield"
            },
            AccountDetails = new AccountDetails
            {
              AccountId = "NRG12345",
              CostPerMonth = 103.75m,
              PayingEnough = false,
              EndOfPlanBalance = 116.46m,
              ElectricityMeterReading = 123456,
              GasMeterReading = 123456
            },
            EnergyUsage = new EnergyUsage
            {
              EnergyUsageId = 1,
              ElectricityCosts = new List<EnergyCost>
              {
                new EnergyCost
                {
                  EnergyCostId = 1,
                  MonthIndex = 1,
                  Cost = 70
                },
                new EnergyCost
                {
                  EnergyCostId = 2,
                  MonthIndex = 2,
                  Cost = 65
                },
                new EnergyCost
                {
                  EnergyCostId = 3,
                  MonthIndex = 3,
                  Cost = 76
                },
                new EnergyCost
                {
                  EnergyCostId = 4,
                  MonthIndex = 4,
                  Cost = 60
                },
                new EnergyCost
                {
                  EnergyCostId = 5,
                  MonthIndex = 5,
                  Cost = 80
                }
              },
              GasCosts = new List<EnergyCost>
              {
                new EnergyCost
                {
                  EnergyCostId = 6,
                  MonthIndex = 1,
                  Cost = 160
                },
                new EnergyCost
                {
                  EnergyCostId = 7,
                  MonthIndex = 2,
                  Cost = 125
                },
                new EnergyCost
                {
                  EnergyCostId = 8,
                  MonthIndex = 3,
                  Cost = 120
                },
                new EnergyCost
                {
                  EnergyCostId = 9,
                  MonthIndex = 4,
                  Cost = 100
                },
                new EnergyCost
                {
                  EnergyCostId = 10,
                  MonthIndex = 5,
                  Cost = 70
                }
              },
              SimilarElectricityCosts = new List<EnergyCost>
              {
                new EnergyCost
                {
                  EnergyCostId = 11,
                  MonthIndex = 1,
                  Cost = 80
                },
                new EnergyCost
                {
                  EnergyCostId = 12,
                  MonthIndex = 2,
                  Cost = 75
                },
                new EnergyCost
                {
                  EnergyCostId = 13,
                  MonthIndex = 3,
                  Cost = 96
                },
                new EnergyCost
                {
                  EnergyCostId = 14,
                  MonthIndex = 4,
                  Cost = 70
                },
                new EnergyCost
                {
                  EnergyCostId = 15,
                  MonthIndex = 5,
                  Cost = 50
                }
              },
              SimilarGasCosts = new List<EnergyCost>
              {
                new EnergyCost
                {
                  EnergyCostId = 16,
                  MonthIndex = 1,
                  Cost = 170
                },
                new EnergyCost
                {
                  EnergyCostId = 17,
                  MonthIndex = 2,
                  Cost = 135
                },
                new EnergyCost
                {
                  EnergyCostId = 18,
                  MonthIndex = 3,
                  Cost = 160
                },
                new EnergyCost
                {
                  EnergyCostId = 19,
                  MonthIndex = 4,
                  Cost = 120
                },
                new EnergyCost
                {
                  EnergyCostId = 20,
                  MonthIndex = 5,
                  Cost = 60
                }
              }
            }
          }
        );

        context.SaveChanges();
      }
    }
  }
}
