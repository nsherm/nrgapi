namespace nrgApi.Database
{
  public class ManagerBase
  {
    protected readonly NrgDbContext Context;

    protected ManagerBase(NrgDbContext context)
    {
      Context = context;
    }
  }
}
