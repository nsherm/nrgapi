using System;
using System.Text;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.EntityFrameworkCore;

namespace nrgApi.Database
{
 
  public class NrgDbContext : DbContext
  {
    private const string Salt = "Ag64dshdsjadasdkj761726";

    public NrgDbContext(DbContextOptions options)
      : base(options)
    {
    }

    public DbSet<Models.Database.UserDetails> Users { get; set; }

    public string HashPassword(string password)
    {
      return Convert.ToBase64String(KeyDerivation.Pbkdf2(
        password,
        Encoding.UTF8.GetBytes(Salt),
        KeyDerivationPrf.HMACSHA1,
        1000,
        256 / 8));
    }
  }
}
