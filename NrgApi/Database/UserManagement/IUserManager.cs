using nrgApi.Models.Database;

namespace nrgApi.Database.UserManagement
{
  public interface IUserManager
  {
    bool Login(string username, string password);
    
    bool Logout(string username);

    UserDetails GetUserDetails(string username);
  }
}
