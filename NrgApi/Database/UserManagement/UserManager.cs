using System.Linq;
using Microsoft.EntityFrameworkCore;
using nrgApi.Models.Database;

namespace nrgApi.Database.UserManagement
{
  public class UserManager : ManagerBase, IUserManager
  {
    public UserManager(NrgDbContext context) : base(context)
    {
    }

    public bool Login(string username, string password)
    {
      var userDetails = Context.Users
        .FirstOrDefault(u => u.Username == username);

      if (userDetails == null || password == null || userDetails.Password != Context.HashPassword(password)) return false;
      
      userDetails.LoggedIn = true;
      Context.SaveChanges();

      return true;
    }

    public bool Logout(string username)
    {
      var userDetails = Context.Users.Find(username);

      if (userDetails == null) return false;
      
      userDetails.LoggedIn = false;
      Context.SaveChanges();

      return true;
    }

    public UserDetails GetUserDetails(string username)
    {
      var userDetails = Context.Users
        .Include(c => c.ContactDetails)
        .Where(u => u.LoggedIn)
        .FirstOrDefault(u => u.Username == username);

      return userDetails;
    }
  }
}
