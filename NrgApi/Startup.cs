﻿using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using nrgApi.Database;
using nrgApi.Database.AccountManagement;
using nrgApi.Database.UserManagement;
using nrgApi.Helpers;

namespace nrgApi
{
  public class Startup
  {
    private IConfiguration Configuration { get; }
    
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }
    
    // This method gets called by the runtime. Use this method to add services to the container.
    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddOptions();
      services.AddMvc();
      
      // configure strongly typed settings objects
      var appSettingsSection = Configuration.GetSection("AppSettings");
      services.Configure<AppSettings>(appSettingsSection);
      var appSettings = appSettingsSection.Get<AppSettings>();
      
      services.AddDbContext<NrgDbContext>(options => options.UseInMemoryDatabase("NrgApi"));
      services.AddSingleton<IJwtHelper, JwtHelper>();
      services.AddScoped<IUserManager, UserManager>();
      services.AddScoped<IAccountManager, AccountManager>();
      
      
      AddJwtAuthentication(services, appSettings);
    }

    private static void AddJwtAuthentication(IServiceCollection services, AppSettings appSettings)
    {
      var key = Encoding.ASCII.GetBytes(appSettings.Secret);

      services.AddAuthentication(x =>
        {
          x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
          x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        })
        .AddJwtBearer(x =>
        {
          x.RequireHttpsMetadata = false;
          x.SaveToken = true;
          x.TokenValidationParameters = new TokenValidationParameters
          {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(key),
            ValidateIssuer = false,
            ValidateAudience = false
          };
        });
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public static void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      app.UseDeveloperExceptionPage();
      app.UseCors(builder => builder.AllowAnyMethod().AllowAnyOrigin().AllowAnyHeader()
        .WithExposedHeaders("Content-Type", "Authorization"));
      app.UseMvc();
      app.UseAuthentication();
    }
  }
}
