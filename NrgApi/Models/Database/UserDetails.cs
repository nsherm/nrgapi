using System.ComponentModel.DataAnnotations;

namespace nrgApi.Models.Database
{
  public class UserDetails
  {
    [Key]
    public string Username { get; set; }
    
    public string Password { get; set; }
    
    public bool LoggedIn { get; set; }

    public int ContactId { get; set; }
    public Contact ContactDetails { get; set; }
    
    public int AccountId { get; set; }
    public AccountDetails AccountDetails { get; set; }
    
    public int EnergyUsageId { get; set; }
    public EnergyUsage EnergyUsage { get; set; }
  }
}
