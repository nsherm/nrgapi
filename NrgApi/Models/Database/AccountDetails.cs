using System.ComponentModel.DataAnnotations;

namespace nrgApi.Models.Database
{
  public class AccountDetails
  {
    [Key]
    public string AccountId { get; set; }
    
    public decimal CostPerMonth { get; set; }
    public bool PayingEnough { get; set; }
    public decimal EndOfPlanBalance { get; set; }
    
    // TODO This would likely reside in its own table in the real world
    public int ElectricityMeterReading { get; set; }
    public int GasMeterReading { get; set; }
  }
}
