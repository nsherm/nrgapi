namespace nrgApi.Models.Database
{
  public class Contact
  {
    public int ContactId { get; set; }
    
    public string FirstName { get; set; }
    public string LastName { get; set; }
    
    public string HouseNumberOrName { get; set; }
    public string Street { get; set; }
    public string Town { get; set; }
  }
}
