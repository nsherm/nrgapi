using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace nrgApi.Models.Database
{
  public class EnergyCost
  {
    [Key]
    public int EnergyCostId { get; set; }
    public int MonthIndex { get; set; }
    public decimal Cost { get; set; }
  }
  
  public class EnergyUsage
  {
    [Key]
    public int EnergyUsageId { get; set; }
    
    [ForeignKey("ElectricityCosts")]
    public ICollection<EnergyCost> ElectricityCosts { get; set; }
    
    [ForeignKey("GasCosts")]
    public ICollection<EnergyCost> GasCosts { get; set; }
    
    [ForeignKey("SimilarElectricityCosts")]
    public ICollection<EnergyCost> SimilarElectricityCosts { get; set; }
    
    [ForeignKey("SimilarGasCosts")]
    public ICollection<EnergyCost> SimilarGasCosts { get; set; }
  }
}
