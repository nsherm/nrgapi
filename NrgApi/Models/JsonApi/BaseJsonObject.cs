﻿using Newtonsoft.Json;

namespace nrgApi.Models.JsonApi
{
    public abstract class BaseJsonObject<T> where T : BaseJsonObject<T>
    { 
        public override string ToString()
        {
            return this.ToJson();
        }
    }

    public static class Serialize
    {
        public static string ToJson<T>(this T self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    public static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            NullValueHandling = NullValueHandling.Ignore
        };
    }
}
