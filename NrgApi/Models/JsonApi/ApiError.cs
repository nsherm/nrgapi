﻿using Newtonsoft.Json;

namespace nrgApi.Models.JsonApi
{
  public class ApiError : BaseJsonObject<ApiError>
  {
    [JsonProperty("errorCode")]
    public string ErrorCode { get; set; }
    
    [JsonProperty("errorMessage")]
    public string ErrorMessage { get; set; }
  }
}
