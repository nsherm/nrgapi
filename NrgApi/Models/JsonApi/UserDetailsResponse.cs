using Newtonsoft.Json;

namespace nrgApi.Models.JsonApi
{
  public class UserDetailsResponse : BaseJsonObject<UserDetailsResponse>
  { 
    [JsonProperty("firstName")]
    public string FirstName { get; set; }
    
    [JsonProperty("houseNumberOrName")]
    public string HouseNumberOrName { get; set; }
    
    [JsonProperty("street")]
    public string Street { get; set; }
    
    [JsonProperty("town")]
    public string Town { get; set; }
  }
}
