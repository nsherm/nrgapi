using Newtonsoft.Json;

namespace nrgApi.Models.JsonApi
{
  public class AccountDetailsResponse : BaseJsonObject<AccountDetailsResponse>
  {
    [JsonProperty("accountId")]
    public string AccountId { get; set; }
    
    [JsonProperty("costPerMonth")]
    public decimal CostPerMonth { get; set; }
    
    [JsonProperty("payingEnough")]
    public bool PayingEnough { get; set; }
    
    [JsonProperty("endOfPlanBalance")]
    public decimal EndOfPlanBalance { get; set; }
    
    [JsonProperty("lastElectricityMeterReading")]
    public int ElectricityMeterReading { get; set; }
    
    [JsonProperty("lastGasMeterReading")]
    public int GasMeterReading { get; set; }
  }
}
