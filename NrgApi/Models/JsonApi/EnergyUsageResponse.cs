using System.Collections.Generic;
using Newtonsoft.Json;

namespace nrgApi.Models.JsonApi
{
  public class EnergyUsageResponse : BaseJsonObject<EnergyUsageResponse>
  {
    [JsonProperty("electricyCosts")]
    public List<decimal> ElectricityCosts { get; } = new List<decimal>();
    
    [JsonProperty("gasCosts")]
    public List<decimal> GasCosts { get; } = new List<decimal>();
    
    [JsonProperty("similarElectricyCosts")]
    public List<decimal> SimilarElectricityCosts { get; } = new List<decimal>();
    
    [JsonProperty("similarGasCosts")]
    public List<decimal> SimilarGasCosts { get; } = new List<decimal>();
  }
}
