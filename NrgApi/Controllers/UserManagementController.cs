using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using nrgApi.Converters;
using nrgApi.Database.UserManagement;
using nrgApi.Helpers;
using nrgApi.Models.JsonApi;

namespace nrgApi.Controllers
{
  public class LoginController : BaseController
  {
    #region Error Codes

    private const string LoginRequestInvalidErrorCode = "NrgApi.v1.loginRequestInvalid";
    private const string LoginRequestErrorCode = "NrgApi.v1.loginRequestError";
    
    private const string UserDetailsRequestInvalidErrorCode = "NrgApi.v1.userDetailsRequestInvalid";
    private const string UserDetailsRequestErrorCode = "NrgApi.v1.userDetailsRequestError";
    
    private const string LogoutRequestInvalidErrorCode = "NrgApi.v1.logoutRequestInvalid";
    private const string LogoutRequestErrorCode = "NrgApi.v1.logoutError";

    #endregion

    #region  Error Messages

    private const string LoginRequestMissingUsernameOrPassword = "You are missing a userId or password";
    private const string LoginRequestInvalid = "Invalid username or password";
    private const string LoginRequestError = "An error occured processing the login request";
    
    private const string LogoutRequestInvalid = "Invalid username or session";
    private const string LogoutRequestError = "An error occured processing the logout request";
    
    private const string UserDetailsRequestInvalid = "Invalid username or session";
    private const string UserDetailsRequestError = "An error occured processing the user details request";

    #endregion

    private readonly IUserManager _userManager;

    public LoginController(IUserManager userManager, IJwtHelper jwtHelper) : base(jwtHelper)
    {
      _userManager = userManager;
    }

    [HttpPost]
    [Route("v1/login")]
    public IActionResult Login([FromBody] LoginRequest request)
    {
      try
      {
        if (!ModelState.IsValid || request == null)
        {
          return StatusCode(StatusCodes.Status401Unauthorized,
            new ApiError
            {
              ErrorCode = LoginRequestInvalidErrorCode,
              ErrorMessage = LoginRequestMissingUsernameOrPassword
            });
        }
        
        if (!_userManager.Login(request.Username, request.Password))
        {
          return StatusCode(StatusCodes.Status401Unauthorized, new ApiError{
            ErrorCode = LoginRequestInvalidErrorCode,
            ErrorMessage = LoginRequestInvalid
          });

        }

        AddJwtToResponseHeaderForUser(request.Username);

        return Ok();

      }
      catch
      {
        return StatusCode(StatusCodes.Status500InternalServerError, new ApiError{
          ErrorCode = LoginRequestErrorCode,
          ErrorMessage = LoginRequestError
        });
      }
    }

    [HttpGet]
    [Route("v1/logout")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public IActionResult Logout()
    {
      try
      {
        if (_userManager.Logout(DetermineUsernameFromJwt()))
        {
          return Ok();
        }

        return StatusCode(StatusCodes.Status401Unauthorized, 
          new ApiError{
            ErrorCode = LogoutRequestInvalidErrorCode,
            ErrorMessage = LogoutRequestInvalid
          });
      }
      catch
      {
        return StatusCode(StatusCodes.Status500InternalServerError, new ApiError{
          ErrorCode = LogoutRequestErrorCode,
          ErrorMessage = LogoutRequestError
        });
      }
    }
    
    [HttpGet]
    [Route("v1/userDetails")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public IActionResult UserDetails()
    {
      try
      {
        var userDetails = _userManager.GetUserDetails(DetermineUsernameFromJwt());
        
        if (userDetails == null)
        {
          return StatusCode(StatusCodes.Status401Unauthorized, new ApiError{
            ErrorCode = UserDetailsRequestInvalidErrorCode,
            ErrorMessage = UserDetailsRequestInvalid
          });
        }
        
        AddJwtToResponseHeader();

        return Ok(UserDetailsConverter.Convert(userDetails));

      }
      catch
      {
        return StatusCode(StatusCodes.Status500InternalServerError, new ApiError{
          ErrorCode = UserDetailsRequestErrorCode,
          ErrorMessage = UserDetailsRequestError
        });
      }
    }
  }
}
