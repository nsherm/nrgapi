using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using nrgApi.Converters;
using nrgApi.Database.AccountManagement;
using nrgApi.Helpers;
using nrgApi.Models.JsonApi;

namespace nrgApi.Controllers
{
  public class AccountDetailsController : BaseController
  {
    #region Error Codes

    private const string AccountDetailsRequestInvalidErrorCode = "NrgApi.v1.accountDetailsRequestInvalid";
    private const string AccountDetailsRequestErrorCode = "NrgApi.v1.accountDetailsError";

    #endregion

    #region  Error Messages

    private const string AccountDetailsRequestInvalid = "Invalid username or session";
    private const string AccountDetailsRequestError = "An error occured processing the account details request";

    #endregion

    private readonly IAccountManager _accountMAnager;

    public AccountDetailsController(IAccountManager accountManager, IJwtHelper jwtHelper) : base(jwtHelper)
    {
      _accountMAnager = accountManager;
    }

    [HttpGet]
    [Route("v1/accountdetails")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public IActionResult GetAccountDetails()
    {
      try
      {
        var accountDetails = _accountMAnager.LookupAccountDetails(DetermineUsernameFromJwt());

        if (accountDetails == null)
        {
          return StatusCode(StatusCodes.Status401Unauthorized, new ApiError
          {
            ErrorCode = AccountDetailsRequestInvalidErrorCode,
            ErrorMessage = AccountDetailsRequestInvalid
          });
        }

        AddJwtToResponseHeader();
        return Ok(AccountDetailsConverter.Convert(accountDetails));
      }
      catch
      {
        return StatusCode(StatusCodes.Status500InternalServerError, new ApiError
        {
          ErrorCode = AccountDetailsRequestErrorCode,
          ErrorMessage = AccountDetailsRequestError
        });
      }
    }
  }
}
