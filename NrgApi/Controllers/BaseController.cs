using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using nrgApi.Helpers;

namespace nrgApi.Controllers
{
  public class BaseController : Controller
  {
    private readonly IJwtHelper _jwtHelper;
    
    public BaseController(IJwtHelper jwtHelper)
    {
      _jwtHelper = jwtHelper;
    }

    private string GetJwt()
    {
      var identity = HttpContext.User.Identity as ClaimsIdentity;
        
      var jwtToken = HttpContext.GetTokenAsync("Bearer", "access_token").Result;
      

      if (jwtToken == null || JwtAboutToExpire(identity))
      {
        jwtToken = _jwtHelper.CreateJwtToken(identity?.Name);
      }

      return jwtToken;
    }
    
    protected string DetermineUsernameFromJwt()
    {
      var identity = HttpContext.User.Identity as ClaimsIdentity;
      return identity?.Name;
    }

    protected void AddJwtToResponseHeader()
    {
      HttpContext.Response.Headers.Add("Authorization", "Bearer " + GetJwt());
    }

    protected void AddJwtToResponseHeaderForUser(string username)
    {
      HttpContext.Response.Headers.Add("Authorization", "Bearer " + _jwtHelper.CreateJwtToken(username));
    }

    private static bool JwtAboutToExpire(ClaimsIdentity identity)
    {
      var expiry = long.Parse(identity?.Claims.First(claim => claim.Type == "exp").Value, CultureInfo.CurrentCulture);
      var expiryDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
      expiryDateTime = expiryDateTime.AddSeconds(expiry);
      var now = DateTime.Now;

      return expiryDateTime.AddMinutes(1) > now;
    }
  }
}
