using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using nrgApi.Converters;
using nrgApi.Database.AccountManagement;
using nrgApi.Helpers;
using nrgApi.Models.JsonApi;

namespace nrgApi.Controllers
{
  public class EnergyUsageController : BaseController
  {
    #region Error Codes
    
    private const string EnergyUsageRequestInvalidErrorCode = "NrgApi.v1.energyUsageRequestInvalid";
    private const string EnergyUsageRequestErrorCode = "NrgApi.v1.energyUsageError";

    #endregion

    #region  Error Messages
    
    private const string EnergyUsageRequestInvalid = "Invalid username or session";
    private const string EnergyUsageRequestError = "An error occured processing the energy usage details request";

    #endregion
    
    private readonly IAccountManager _accountMAnager;
    
    public EnergyUsageController(IAccountManager accountManager, IJwtHelper jwtHelper) : base(jwtHelper)
    {
      _accountMAnager = accountManager;
    }
    
    [HttpGet]
    [Route("v1/energyusage")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public IActionResult GetEnergyUsage()
    {
      try
      {
        var energyUsage = _accountMAnager.LookupEnergyUsage(DetermineUsernameFromJwt());
        
        if (energyUsage == null)
        {
          return StatusCode(StatusCodes.Status401Unauthorized, new ApiError
          {
            ErrorCode = EnergyUsageRequestInvalidErrorCode,
            ErrorMessage = EnergyUsageRequestInvalid
          });
        }
        
        AddJwtToResponseHeader();
        
        return Ok(AccountDetailsConverter.Convert(energyUsage));

      }
      catch
      {
        return StatusCode(StatusCodes.Status500InternalServerError, new ApiError
        {
          ErrorCode = EnergyUsageRequestErrorCode,
          ErrorMessage = EnergyUsageRequestError
        });
      }
    }
  }
}
