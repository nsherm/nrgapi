using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using nrgApi.Database;

namespace nrgApi
{
  public static class Program
  {
    public static void Main(string[] args)
    {
      var host = BuildWebHost(args);
      
      using (var scope = host.Services.CreateScope())
      {
        var services = scope.ServiceProvider;

        DataGenerator.Initialize(services);
      }
      
      host.Run();
    }

    private static IWebHost BuildWebHost(string[] args)
    {
      var webHostBuilder = WebHost.CreateDefaultBuilder(args)
        .UseStartup<Startup>();

      webHostBuilder.UseKestrel();

      return webHostBuilder.Build();
    }
  }
}
