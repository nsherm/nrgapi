namespace nrgApi.Helpers
{
  public interface IJwtHelper
  {
    string CreateJwtToken(string userId);
  }
}
