using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace nrgApi.Helpers
{
  public class JwtHelper : IJwtHelper
  {
    private readonly AppSettings _appSettings;
    
    public JwtHelper(IOptions<AppSettings> appSettings)
    {
      _appSettings = appSettings.Value;
    }
    
    public string CreateJwtToken(string userId)
    {
      var tokenHandler = new JwtSecurityTokenHandler();
      var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
      var tokenDescriptor = new SecurityTokenDescriptor
      {
        Subject = new ClaimsIdentity(new[]
        {
          new Claim(ClaimTypes.Name, userId)
        }),
        Audience = "NRG",
        // TODO Set Issuer address
        // Issuer = "NRGAPI",
        Expires = DateTime.UtcNow.AddMinutes(15), // TODO Move out of code
        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
          SecurityAlgorithms.HmacSha256Signature)
      };
      var token = tokenHandler.CreateToken(tokenDescriptor);
      
      return tokenHandler.WriteToken(token);
    }
    
  }
}
